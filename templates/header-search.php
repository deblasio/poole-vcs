<header class="banner search">
<div class="banner-graphic solid-color">
  <div class="container">

    <nav class="nav-primary navbar navbar-default">
     <span class="glyphicon glyphicon-menu-hamburger  menu-toggle" aria-hidden="true"></span>
 
    </nav>
<div class="page-header">
  <div class="page-header_content">
  <h1 class="search-h1"><?php the_title();?></h1>
      <?php echo do_shortcode( '[web_proc]' ); ?>
  </div>

  <svg class="search-graphic" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  viewBox=" -6 -2 30 22">
<path stroke="#ffffff" fill="none" d="M18.869 19.162l-5.943-6.484c1.339-1.401 2.075-3.233 2.075-5.178 0-2.003-0.78-3.887-2.197-5.303s-3.3-2.197-5.303-2.197-3.887 0.78-5.303 2.197-2.197 3.3-2.197 5.303 0.78 3.887 2.197 5.303 3.3 2.197 5.303 2.197c1.726 0 3.362-0.579 4.688-1.645l5.943 6.483c0.099 0.108 0.233 0.162 0.369 0.162 0.121 0 0.242-0.043 0.338-0.131 0.204-0.187 0.217-0.503 0.031-0.706zM1 7.5c0-3.584 2.916-6.5 6.5-6.5s6.5 2.916 6.5 6.5-2.916 6.5-6.5 6.5-6.5-2.916-6.5-6.5z"></path>
</svg>
  <div class="clearfix"></div>
</div>

  </div>
  </div>
</header>
