

<div class="left-content-frame">
<?php 
        if (get_field('image')){

                $style = 'style="background-image:url(\''.get_field('image').'\');background-size:cover;"';
        }

        the_content(); 

        ?>
</div>
<div class="right-content-frame" >
    <div class="rcf-main" <?php echo $style;?> > </div>
    <div class="rcf-additional"><?php echo get_field("additional_content")?> </div>
</div>

