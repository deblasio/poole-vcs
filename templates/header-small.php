<header class="banner">
<div class="banner-graphic solid-color">
  <div class="container">

    <nav class="nav-primary navbar navbar-default">
     <span class="glyphicon glyphicon-menu-hamburger  menu-toggle" aria-hidden="true"></span>
 
    </nav>
<div class="page-header">
  <div class="page-header_content">
  <h1><?php the_title();?></h1>
  </div>
</div>

  </div>
  </div>
</header>
