<header class="banner">
<div class="banner-graphic">
  <div class="container">

    <nav class="nav-primary navbar navbar-default">
     <span class="glyphicon glyphicon-menu-hamburger menu-toggle" aria-hidden="true"></span>
 
    </nav>
<div class="page-header">
  <div class="page-header_content">
  <span class="title one">The</span><span class="title two">  Volunteer </span><span class="title three"> Poole</span>
  <div class="subtitle">
  
    <span class="subtitle-text">Connecting Charities with </span>
   <span class="subtitle-text">Businesses and Volunteers </span>
   </div>
  </div>
  <!--<img src="<?= get_template_directory_uri(); ?>/dist/images/peep.svg" class="main-graphic"> -->
</div>

  </div>
  </div>


</header>
