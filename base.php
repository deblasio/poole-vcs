<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body id="poolecvs-body" <?php body_class(); ?>>

<div id="menu" class="menu-container">
  

  <div id="item-2" class="menu-item blue">
     <h1>Menu</h1>
    <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/search">Search for Roles</a></li>
     <li><a href="/about">About us</a></li>
    </ul>
  </div>
  <div id="item-2" class="menu-item d-blue">
    <h1>Businesses</h1>
    <div class="menu-image m1"></div>
    <div class="menu-content">
    <p>Want to enhance employee engagement, widen your professional network, and strengthen your presence your community?</p>
    </div>
    <a href="#">More information</a>
  </div>
  <div id="item-3" class="menu-item green">
    <h1>Charities</h1>
    <div class="menu-image fe"></div>    
    <div class="menu-content">
    <p> Want to attract skilled volunteers in Poole and Dorset or discover opportunities for corporate support?</p>
    </div>
    <a href="#">More information</a>
  </div>
  <div id="item-4" class="menu-item menu-final yellow">
    <span class="glyphicon glyphicon glyphicon-remove menu-toggle" aria-hidden="true"></span>
    <h1>Volunteers</h1>    
    <div class="menu-image fe2"></div>    
    <div class="menu-content">
    <p>Want to make a difference to people and give something back to your community, all whilst developing new skills and making new friends?</p>
    </div>
    <div>
    <a href="#">More information</a>
    </div>
  </div>

</div>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
    if (strpos(Wrapper\template_path(),'index.php')!==false){
       get_template_part('templates/header');
    }else if (strpos(Wrapper\template_path(),'search')!== false){      
      get_template_part('templates/header-search');
    }else{
     
      get_template_part('templates/header-small');
    }

    ?>
    <div class="wrap container" role="document">
      <div class="content row">
        <main class="main <?php echo Wrapper\template_path(); ?>">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
      
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
