<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <div class="jobs_list">
  <div class="box-sizer"></div>
  <?php get_template_part('templates/content', 'page-wide-search'); ?>
</div>
<?php endwhile; ?>