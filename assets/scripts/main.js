/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
var l_search_engine = "";


function DropDown(el) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.dropdown > li');
    this.val = '';
    this.index = -1;
    this.texts=[];
    this.placeholder_text =this.dd.children('span').text();
    this.initEvents();
}
DropDown.prototype = {
    initEvents : function() {
        var obj = this;

        obj.dd.on('click', function(event){
            jQuery(this).toggleClass('active');
            console.log('click')
            return true; 
        }); 

        obj.opts.on('click',function(){
            var opt = jQuery(this);
            opt.children('input').prop('checked', function( i, val ) {

                console.log(val)
                if (!val){
                    obj.push(opt.index(),opt.text());
                    obj.placeholder.text(obj.getValue());
                    opt.addClass("active");
                }else{
                    obj.pop(opt.index(),opt.text());
                    obj.placeholder.text(obj.getValue());
                    opt.removeClass("active");
                }  
                return !val;
            });
            
            

            l_search_engine.reload();
        return false;
        });
    },
    getValue : function() {
        var text_value = "";
        var text_count = 0;

        jQuery.each(this.texts,function(i,val){
            if (val!==undefined){
            text_value = text_value + val;
            text_count++
            }
        });

        if (text_count> 1){
            return text_count+" items selected"
        }else if (text_count===0){
            return this.placeholder_text;
        }


        return text_value;
    },
    getIndex : function() {
        return this.val;
    },
    push:function(index,value){
        this.texts[index]=value;
    }, 
    pop:function(index,value){
        this.texts[index]=undefined;
    }

};
(function($) {

   $(document).click(function(event) { 
       $('.wrapper-dropdown').each(function(){
        console.log(!$(event.target).closest(this).length);
            if(!$(event.target).closest(this).length) {        
                $(this).removeClass('active');     
            }   

       });

   
    })

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': { 
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.

(function($) {
  $(document).ready(function(){
$('.grid').masonry({
  itemSelector: '.box-outer',
  columnWidth: '.box-sizer'
});
});
$('.box').hover(
       function(){ $(this).addClass('hover') },
       function(){ $(this).removeClass('hover') }
);


$('.wrapper-dropdown').each(function(index, value){var dd = new DropDown($(value))}); 

})(jQuery);





/*LOCAL JS*/


//if(window.location.href.indexOf("register.html") > -1 ){
//        $('.no-underline.header-large').text('Register');
//    }


function formatDate(d) {
    var month = d.getMonth();
    var day = d.getDate();
    month = month + 1;
    month = month + "";
    if (month.length == 1) {
        month = "0" + month;
    }
    day = day + "";
    if (day.length == 1) {
        day = "0" + day;
    }
    return day + '/' + month + '/' + d.getFullYear();
}




/*cookie functions*/
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}
var hs_texts = "";




/************************************************/
/************************************************/
/*  Template for Processing the Classifications
/*************************************************/
function searchengine() {

    this.l_start = 0;
    this.l_end = 10;
    this.page_size = 10;
    this.current_page = 1;
    this.searchFormName;
    this.xhr;
    this.cookieSaveEnabled = false;
    this.cookieName = 'ajax_search_cookie';
    this.enableLoadWithNoState = false;
    this.reloadEnabled = false;
    var $ = jQuery;
    this.formURL;
    this.contentArea;
    this.form;
    this.visibleClassifications = [];
    this.JPages = false;
    this.navDiv = "";
    this.JPagesContainer = "";
    this.JPagesCount = "";
    this.chosen = false;
    this.bbq = false;
    this.searchButtonClass = ".search_ajaxss .submit";
    this.no_matching_jobs_text = "<p>Unfortunately we currently have no positions that match your search criteria.</p>";
    this.loading_image_html =  '<div class="article-image-loading"><div class="article-image-loading-thingy"><div class="fslb-loading-circle">  </div></div></div>';
    

    this.init = function (config) {

        this.no_matching_jobs_text = hs_texts.srch_no_hits_text || this.no_matching_jobs_text

        /*Combine the config with default values*/
        for (var attrname in config) {
            this[attrname] = config[attrname];
        }
        /*set up for external or internal*/
        this.checkInternalExternal();

        /*Upgrade the select boxes to use multiple selects*/
        this.upgradeSelectBoxes();

        /*upgrade the text field*/
        this.textSearchField = this.form.find("#p_text");
        this.textSearchField.attr('placeholder', 'Enter Keywords');

        /*Enable the typing timer */
        if (this.enableTypetimer) {
            this.initTypingTimer();
        }

        /*enable the search field*/
        this.createSearchButton();

        if (this.chosen) {
            this.initChosen('select[multiple="multiple"] , .search_ajaxss select', true);
        }

        if (this.bbq) {
            this.initBBQ();
        }

        this.reloadEnabled = true;

    }

    this.createSearchButton = function () {
        var _this = this;
        $(_this.searchButtonClass).click(function (e) {
            e.preventDefault();
            _this.reload();
        });
    }

    this.checkInternalExternal = function () {
        if (this.form.find("#publishTo").val() === 'WWW') {
            this.external = true;
        } else {
            this.external = false;
        }
    }

    this.initChosen = function (className, refreshOnSelect) {
        _this = this;
        $(className).chosen({
            display_disabled_options: false,
            width: "100%"
        });
        if (refreshOnSelect) {
            $(className).bind('change', function (event, ui) {
                if (_this.reloadEnabled) {
                    _this.reload();
                }
            });
        }
    }

    this.upgradeSelectBoxes = function () {
        this.form.find("select").each(function () {
            var select = $(this);
            select.attr("multiple", "multiple");
            var option = select.find("option").first();
            select.attr('data-placeholder', option.text());
            option.remove();
        });
    }


    this.initTypingTimer = function () {

        _this = this;
        /*setup before functions*/
        var typingTimer; /*timer identifier*/
        var doneTypingInterval = 1000; /*time in ms,*/

        /*prevent the ability for the user to press enter on the form*/
        $('#p_text').keypress(function (event) {
            if (event.keyCode == 10 || event.keyCode == 13) {
                event.preventDefault();
                _this.reload();
            }
        });

        /*on keyup, start the countdown*/
        $("#p_text").keyup(function () {
            clearTimeout(typingTimer);

              if ($("#p_text").val) {
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            }
        });

        /*user is "finished typing," do something*/
        function doneTyping() {
           if ($("body").hasClass("vacancies")) {
        _this.reload();
        }

    }}


    this.enableJPages = function (nav, container, count) {
        this.JPages = true;
        this.navDiv = nav;
        this.JPagesContainer = container;
        this.JPagesCount = count;
    }

    this.initJPages = function () {

        $(this.navDiv).jPages({
            containerID: this.JPagesContainer,
            first: false,
            previous: false,
            next: "next",
            last: "last",
            perPage: this.JPagesCount,
            minHeight: false,
            callback: function (pages, items) {
                $('html').animate({
                    scrollTop: 0
                }, 1); /*IE, FF*/
                $('body').animate({
                    scrollTop: 0
                }, 1); /*chrome,*/
          
                
                var grid = $('.jobs_list');
                 $(grid).masonry('destroy');
                
                    grid.masonry({
                        itemSelector: '.box-outer',
                        columnWidth: '.jobpost',
                        gutter: 20,                        
                        columnWidth: '.box-sizer'
                    })
                    
            
           
            }
        });

    }


    this.initBBQ = function () {
/*

        var form = $(this.form);
        /*get the state from the url
        var _this = this;
        var cur_state = $.bbq.getState("filter");
        if ((typeof cur_state === 'undefined') && this.cookieSaveEnabled) {
            if (!_this.enableLoadWithNoState) {
                _this.reloadEnabled = false;
            }
            cur_state = getCookie(this.cookieName);

        }
        var form_data = $.deparam.querystring(cur_state);

        $.each(form_data, function (k, v) {
            if (typeof v === 'string') {
                v = [v];
            }

            $.each(v, function (x, val) {
                if (k === 'p_search') {
                    $('#p_text').val(v);

                } else {
                    var select = $('select[name=p_category_code_arr]', form).val(val);
                    select.data();
                }
            });
          
        });

        jQuery(".search_ajaxss select").trigger("change");
        jQuery(".search_ajaxss select ").trigger("chosen:updated.chosen");

        if (!_this.enableLoadWithNoState) {
            _this.reloadEnabled = false;
        }

        if ($('.search_ajaxss input[type="radio"]input[checked="checked"]').length === 0) {
            jQuery(".search_ajaxss select").trigger("chosen:updated.chosen");
        }

        if (!_this.enableLoadWithNoState) {
            _this.reloadEnabled = true;
        }*/
    }

    this.updateCookieState = function () {
        setCookie(this.cookieName, $(this.form).serialize(), 10);
    }

    this.updateState = function () {

        if (this.cookieSaveEnabled) {

            this.updateCookieState();
            $.bbq.pushState({
                filter: $(this.form).serialize()
            });
        } else {

            $.bbq.pushState({
                filter: $(this.form).serialize()
            });

        }

    }

    this.gotoPage = function (page) {
        this.l_start = (page_size * page) - 10;
        this.l_end = (page_size * page);
    }

    this.resetPage = function (page) {
        this.l_start = 0;
        this.l_end = page_size;
    }

    this.readSearchHash = function () {
        var searchHash;
        if (window.location.hash) {
            try {
                searchHash = location.hash.match(new RegExp('p_search' + '%20([^&]*)'))[1];
            } catch (e) {}

        }
    }

    this.submit = function () {

        this.updateState();
        document.location.href = "/current_vacancies.html#" + $.param.fragment();
    }


    /* Ajax request to display XML Job feed */
    /*--------------RELOAD------------------*/
    this.reload = function (reset) {
       
        /*Call the ajax method to process the xml file.*/
        var _this = this;
        if (this.xhr) {
            this.xhr.abort();
        }
        if (reset === true) {
            this.resetPage();
        }
        /* process form data*/ 

        var processed_form_data = _this.form.serializeArray();
        $.each(processed_form_data, function (k, v) {

            if (v.name === "p_search") {
                if (v.value !== '') {
                    v.value.replace(" ", "%");
                    v.value = "%" + v.value + "%";
                }
            }
        });

        processed_form_data = $.param(processed_form_data);

        this.xhr = $.ajax({
            
          
            type: "GET",
            url: _this.formURL,
            dataType: 'json',
          
            data: processed_form_data + '&p_summary=Y',

            beforeSend: function () {
                _this.contentArea.html(_this.loading_image_html);
            },

            success: function (xml) {
                 console.log('RESET');

                var l_count = 0;
                var l_content = "";
                var l_closing_date = "";
                var l_date_str = "";
                var closed_date;
                 totalResult = xml.total;
                console.log(totalResult);

                $(xml.jobs).each(function () {

                    /*Show external or internal job dates*/
                    if (this.external) {
                        l_closing_date = this.publication.internet.closing_date;
                    } else {
                        l_closing_date = this.publication.internet.closing_date;
                    }

                    l_date_str = l_closing_date;
                    closed_date = '';

                    try {
                        closed_date = formatDate($.datepicker.parseDate("yy-mm-dd", l_date_str.split(' ')[0]));
                    } catch (e) {
                        closed_date = '';
                    }

                    /* find the node category and start loop  */
                    l_count++;
                    l_content = l_content + processBodyTemplate(this, processClassificationTemplate(this, closed_date), processButtonsTemplate(this));

                });

                 $('.type-mega.type-border').html('<span>Search returned ' + l_count + " matches");
        
        if (l_count === 0) {
          l_content = _this.no_matching_jobs_text;
                   $('.type-mega.type-border').html('<span>Search returned ' + l_count + " matches");
        }


                /*add content string to search content window*/
                _this.contentArea.html('<div class="box-sizer"></div>'+l_content);

                if (_this.JPages) {
                    _this.initJPages();
                }
                _this.updateState();
            },

            /*Define error callback to handle the error*/
            error: function (e, b, c) {
                _this.contentArea.html("Sorry something went wrong.");
            }
        }).done(
            function () { //Once done begin initialising masonry
                   var statusUD;
                console.log(typeof totalResult);
                
                if(totalResult == 1){
                        statusUD = totalResult + ' role found';
                }else{
                        statusUD = totalResult + ' roles found';
                }
               
            console.log(statusUD);
            $('.total-jobs').html(statusUD);
                
      var grid = $('.jobs_list');
                $(grid).masonry('destroy');
      grid.masonry({
        itemSelector: '.jobpost_wrapper',
        columnWidth: '.box-sizer'
      })
      
      
      $(grid).masonry('layout');
            $(grid).masonry('reloadItems');

      $(".jobpost_wrapper").hover(function (e) {
        $(this).toggleClass('hover');
      });}
            
    
        )
         
            
            
       
    }; /* JSON Ajax END */



    /*  Process the  classifications template
  ---------------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------------*/
  function processClassificationTemplate(job, closed_date) {

    var l_content = '<p class="classifications">';
    var l_values = "";

    $.each(job.classifications, function () {
      $.each(this.values, function () {
        l_values = l_values + this.class_val;
      });
      if (l_values !== "") {
        l_content = l_content + '<span class="jobclass"><span class="jobclass_type">' + this.name + ':</span> <span class="jovalue">' + "&nbsp;" + l_values + '</span></span>';
        l_values = "";
      }
    });
    /*Add reference number and closing date*/
    l_content = l_content + '<span class="jobclass"><span class="jobclass_type">Ref No:</span> <span class="class_value">' + "&nbsp;" + job.refno + '</span></span>';

    if (closed_date !== "") {
      l_content = l_content + '<span class="jobclass"><span class="jobclass_type">Closing Date</span>: <span class="class_value">' + closed_date + '</span></span>';
    }

    return l_content;
  }


    /*  Process the The buttons template    
  ---------------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------------*/
function processButtonsTemplate(job) {
    return '</div><div class="jobpost_nav"><span class="link_primarycolour">Read more</span></div>';
  }



  /*  Process the The job template    
  ---------------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------------*/
  function processBodyTemplate(job, content, buttons) {
    return '<a href="' + job.weblink.substr(job.weblink.indexOf('m/') + 1)+ '" class="jobpost_wrapper box-outer" id="' + job.id + '">' + '<div class="jobpost job_postings wow animated fadeInUpBig">' + '<div class="jobpost_body"> <h2 class="primarycolour_20"><span>' + job.title + '</span></h2><p>' + job.summary.substring(0, 100) + '...</p>' + content + buttons + '</div>' + '</a>';
  }

}


/*  Document initialisation
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------*/

jQuery(document).ready(function ($) {


    var emptyVal = 'All';
    var convertFileUploads = false;
    var convertHelpToTooltips = false;
    var listItemsAutoWidth = true;
    var form = $('.search_ajaxss');
    var isJobsListPage = $("body").hasClass("vacancies");
    var contentArea = $('#jobs-list');
    var contentAreaTwo = $('#icams_inserted');
    var formUrl = form.attr('action');

    if(window.location.href.indexOf('search')>0){
    l_search_engine = new searchengine();

    l_search_engine.init({
        formURL: form.attr('action'),
        contentArea: $('.jobs_list'),
        form: form,
        cookieSaveEnabled: true,
        enabled: $("body").hasClass("page"),
        enableTypetimer: false,
        chosen: false,
        bbq: true,
        visibleClassifications: []
    });

    /*l_search_engine.enableJPages('.jobs-nav', 'jobs_list', 6);*/
    l_search_engine.reload();
   /* l_search_engine.readSearchHash();*/
    }
 
   
});
