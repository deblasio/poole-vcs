/* 
 ** Forked code from https://github.com/jmar777/kwicks/blob/master/jquery.kwicks.js 
 ** Pulled expand code
 ** Added toggle functionality
 */
(function() {

  this.Menu = function(el, btn) {
    /* Globals */
    this.jQuerycontainer = jQuery(el),
      this.button = jQuery(btn),
      this.jQuerymenuItems = this.jQuerycontainer.children(),
      this.selectedIndex = this.jQuerymenuItems.filter('.kwicks-selected').index(),
      this.expandedItem = this.selectedIndex,
      /* Get dimensions */
      this.calculateItemSizes(),
      this.offsets = this.getOffsetsForExpanded(),
      this.primaryDimension = 'width',
      this.secondaryDimension = 'height',
      this.primaryAlignment = 'left',
      this.secondaryAlignment = 'right',
      this.isAnimated = false,
      this.jQuerytimer = jQuery({
        progress: 0
      }),
      this.updateStyle(),
      this.open = false,
      /* Start events */
      this.initEvents();
    this.initWindowResizeHandler();

  }
  Menu.prototype.getCollapsedItem = function() {
    return -1 === this.expandedItem ? [] : this.jQuerymenuItems.not(this.getExpandedItem()).get();
  };

  Menu.prototype.getExpandedItem = function() {
    return this.jQuerymenuItems[this.expandedItem] || null;
  };

  Menu.prototype.getContainerSize = function(bool) {
    return this.jQuerycontainer.outerWidth(bool);
  };

  Menu.prototype.getRandomIndex = function() {
    var index = Math.floor(Math.random() * this.jQuerymenuItems.length);
    var val = this.jQuerymenuItems[index];
    // now remove that value from the array
    this.jQuerymenuItems.splice(index, 1);
    return val;
  };

  Menu.prototype.calculateItemSizes = function() {
    var containerSize = this.getContainerSize(true),
      numItems = this.jQuerymenuItems.length;
    this.itemSize = containerSize / numItems;
    this.itemMaxSize = containerSize / 3 * 2;

    // Set max size
    if (numItems < 5) {
      this.itemMaxSize = containerSize / 3 ;
    } else {
      this.itemMaxSize = containerSize / 3;
    }
    // Using max set the min size 
    this.itemMinSize = (containerSize - this.itemMaxSize) / (numItems - 1);
  };

  Menu.prototype.expand = function(index) {
    var self = this,
      // used for expand-complete event later on
      oldIndex = this.expandedItem,
      oldExpanded = this.getExpandedItem();

    if (this.jQuerycontainer.width() < 768) {
      var jQueryitems = this.jQuerymenuItems;
      for (var i = jQueryitems.length; i--;) {
        this.setStyle(jQueryitems[i], 'width:100%');
      }
      return;
    }

    if (index === -1) index = this.selectedIndex;

    if (index === this.expandedItem) return;

    jQuery(this.getExpandedItem()).removeClass('menu-expanded');
    jQuery(this.getCollapsedItem()).removeClass('menu-collapsed');
    this.expandedItem = index;
    jQuery(this.getExpandedItem()).addClass('menu-expanded');
    jQuery(this.getCollapsedItem()).addClass('menu-collapsed');

    // handle panel animation
    var jQuerytimer = this.jQuerytimer,
      numPanels = this.jQuerymenuItems.length,
      startOffsets = this.offsets.slice(),
      offsets = this.offsets,
      targetOffsets = this.getOffsetsForExpanded();

    jQuerytimer.stop()[0].progress = 0;
    this.isAnimated = true;
    jQuerytimer.animate({
      progress: 1
    }, {
      duration: 300,
      step: function(progress) {
        if (self._dirtyOffsets) {
          offsets = self.offsets;
          targetOffsets = self.getOffsetsForExpanded();
          self._dirtyOffsets = false;
        }
        offsets.length = 0;
        for (var i = 0; i < numPanels; i++) {
          var targetOffset = targetOffsets[i],
            newOffset = targetOffset - ((targetOffset - startOffsets[i]) * (1 - progress));
          offsets[i] = newOffset;
        }
        self.updateStyle();
      },
      complete: function() {
        self.isAnimated = false;
        self.jQuerycontainer.trigger('expand-complete.kwicks', { 
          index: index,
          expanded: self.getExpandedItem(),
          collapsed: self.getCollapsedItem(),
          oldIndex: oldIndex,
          oldExpanded: oldExpanded,
          isAnimated: false
        });
      }
    });
  };

  Menu.prototype.getOffsetsForExpanded = function() {
    var expandedIndex = this.expandedItem,
      numItems = this.jQuerymenuItems.length,
      size = this.itemSize,
      minSize = this.itemMinSize,
      maxSize = this.itemMaxSize;

    //first panel is always offset by 0
    var offsets = [0];

    for (var i = 1; i < numItems; i++) {
      // no panel is expanded
      if (expandedIndex === -1) {
        offsets[i] = i * (size);
      }
      // this panel is before or is the expanded panel
      else if (i <= expandedIndex) {
        offsets[i] = i * (minSize);
      }
      // this panel is after the expanded panel
      else {
        offsets[i] = maxSize + (minSize * (i - 1)) + (i);
      }
    }
    return offsets;
  };

  Menu.prototype.updateStyle = function() {
    var offsets = this.offsets,
      jQueryitems = this.jQuerymenuItems,
      pDim = this.primaryDimension,
      pAlign = this.primaryAlignment,
      sAlign = this.secondaryAlignment,
      spacing = this.panelSpacing,
      containerSize = this.getContainerSize(true);

    var stylePrefix = !!this._stylesInited ? '' : 'position:absolute;',
      offset, size, prevOffset, style;
    // loop through remaining panels
    for (var i = jQueryitems.length; i--;) {
      prevOffset = offset;
      offset = Math.round(offsets[i]);
      if (i === jQueryitems.length - 1) {
        size = containerSize - offset;
        style = sAlign + ':0;' + pDim + ':' + size + 'px;';
      } else {
        size = prevOffset - offset;
        style = pAlign + ':' + offset + 'px;' + pDim + ':' + size + 'px;';
      }

      this.setStyle(jQueryitems[i], stylePrefix + style);
    }

    if (!this._stylesInited) {
      this.jQuerycontainer.addClass('menu-processed');
      this._stylesInited = true;
    }
  };

  Menu.prototype.setStyle = (function() {
    if (jQuery.support.style) {
      return function(el, style) {
        el.setAttribute('style', style);
      };
    } else {
      return function(el, style) {
        el.style.cssText = style;
      };
    }
  })();

  Menu.prototype.resize = function() {
    if (this.jQuerycontainer.width() > 768) {
      this.jQuerycontainer.removeClass('menu-horizontal');
      this.calculateItemSizes();
      this.offsets = this.getOffsetsForExpanded();

      if (this.isAnimated) {
        this._dirtyOffsets = true;
      } else {
        // otherwise update the styles immediately
        this.updateStyle();
      }
    } else {
      this.jQuerycontainer.addClass('menu-horizontal');
      var jQueryitems = this.jQuerymenuItems;
      for (var i = jQueryitems.length; i--;) {
        this.setStyle(jQueryitems[i], 'width:100%;');
      }
    }
  };

  Menu.prototype.initWindowResizeHandler = function() {
    var self = this,
      prevTime = 0,
      execScheduled = false,
      jQuerywindow = jQuery(window);

    var onResize = function(e) {
      if (!e) {
        execScheduled = false;
      }
      var now = +new Date();
      if (now - prevTime < 20) {
        if (execScheduled) return;
        setTimeout(onResize, 20 - (now - prevTime));
        execScheduled = true;
        return;
      }
      prevTime = now;
      self.resize();
    };
    jQuerywindow.on("resize", onResize);
  };

  Menu.prototype.toggle = function() {
    // Hide menu items  
    var jQuerycontainer = this.jQuerycontainer,
      jQueryitems = this.jQuerymenuItems,
      self = this;
    jQuerycontainer.toggleClass('open');
    
    this.resize();
    
    jQuery(this.jQuerymenuItems).each(function(i) {
      var el = jQuery(this);
      setTimeout(function() {
        if (jQuerycontainer.hasClass('open')) {
          el.addClass('active');
        } else {
          el.removeClass('active');
        }
      }, i * 100);
    }); 
  }

  Menu.prototype.initEvents = function(config) {
    var self = this;
    jQuery(this.button).on('click', function() {
      self.toggle();
      jQuery(this).toggleClass('project-open');
    });
    jQuery(this.jQuerymenuItems).on('mouseenter', function() {
      self.expand(jQuery(this).index());
    });
    jQuery(this.jQuerycontainer).on('mouseleave', function() {
      self.expand(-1);
    });
    jQuery(document).keyup(function(e) {
      if (e.keyCode == 27) {
        self.toggle();
        jQuery(this).toggleClass('project-open');
      }
    });
  }
}());

var myMenu = new Menu(jQuery('#menu'), jQuery('.menu-toggle,.menu-final:before')); 