<?php get_template_part('templates/page', 'header'); ?>
<div class="grid">
<div class="box-sizer"></div>
<?php 

// query
$the_query = new WP_Query(array(
    'post_type'         => 'post',
    'posts_per_page'    => -1,    
    'meta_key'          => 'seq',
    'orderby'           => 'meta_value_num',
    'order'             => 'ASC'
));



if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) {

        $the_query->the_post();
       if(get_field('social')){
           ?>  <div class="box-outer  social<?php echo get_field('size');?>">       

            <div class="box social<?php echo get_field('color');?>">
            <div class="social-box google"></div>
            <div class="social-box twitter"></div>
            <div class="social-box youtube"></div>
            <div class="social-box facebook"></div>
       </div>
        </div>

    <?php
      }else{ 
        if (get_field('image')){
          $style = 'style="background-image:url(\''.get_field('image').'\');background-size:cover;"';
        }
    ?>

        <div id="<?php echo the_id();?>" class="box-outer  <?php echo get_field('size');?>">       
          <div class="box <?php echo get_field('color');?>">

    <?php
      /* if there is an image print the image box*/
      if (get_field('image')){
               echo  '<div class="image-box" '.$style.'></div>';
               echo '<div class="side-text">';
               the_title('<p>','</p>');
               echo '</div>';
      }else{
        the_content();
      }

    ?>
        </div>
      </div>
      <?php if (get_field('page_link')){?>
      <script type="text/javascript">

        jQuery(document).ready(function(){ jQuery("#<?php echo the_id();?>").on("click",function(){
           document.location.href="<?php echo get_field('page_link');?>";

        })})


      </script>
      <?php
      }

    }
        //
        // Post Content here
        //
    } // end while
} // end if
?>
</div>

<?php// the_posts_navigation(); ?>
